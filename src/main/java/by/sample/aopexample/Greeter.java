package by.sample.aopexample;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Greeter {
    private static final String GREETING = "Hello from Aspect!";

    @Pointcut("@annotation(GreetMe)")
    public void hijackMethod() {}

    @Around("hijackMethod()")
    public Object sayGreet(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println(GREETING);

        Object result = joinPoint.proceed();

        System.out.println(GREETING);

        return result;
    }
}
