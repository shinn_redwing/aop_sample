package by.sample.aopexample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class InformerImpl implements Informer {
    private static final Logger LOGGER = LoggerFactory.getLogger(InformerImpl.class);

    @Override
    @GreetMe
    public void inform() {
        LOGGER.info("Very important information");
    }
}
