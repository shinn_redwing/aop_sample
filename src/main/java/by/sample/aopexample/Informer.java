package by.sample.aopexample;

public interface Informer {
    void inform();
}
