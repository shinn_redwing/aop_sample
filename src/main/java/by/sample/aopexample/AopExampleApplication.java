package by.sample.aopexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class AopExampleApplication {

	public static void main(String[] args) {
		ApplicationContext appCtx = SpringApplication.run(AopExampleApplication.class, args);

		appCtx.getBean(Informer.class).inform();
	}
}
